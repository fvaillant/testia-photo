/**
 * Angular
 */
import { Injectable } from '@angular/core';

/**
 * Service for credentials
 * @author Fabrice VAILLANT
 */
@Injectable()
export class CredentialService {

    /**
     * Valids crentials
     * @author Fabrice VAILLANT
     * @param login : login to control
     * @param password : password to control
     * @returns true if credentials are valid
     */
    validCrentials(login: string, password: string): boolean {
        if (login === 'admin' && password === 'test') {
            return true;
        }
        return false;
    }
}
