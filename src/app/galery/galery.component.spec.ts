import { TestBed, async } from '@angular/core/testing';
import { GaleryComponent } from './galery.component';

describe('GaleryComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        GaleryComponent
      ],
    }).compileComponents();
  }));

  it('should create the galery component', () => {
    const fixture = TestBed.createComponent(GaleryComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should render nav bar title as 'Galerie'`, () => {
    const fixture = TestBed.createComponent(GaleryComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.navbar-brand').textContent).toContain('Galerie');
  });
});
