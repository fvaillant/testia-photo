/**
 * Tests
 */
import { TestBed, async } from '@angular/core/testing';

/**
 * Components
 */
import { ConnectComponent } from './connect.component';

/**
 * Angular Module
 */
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

/**
 * Services
 */
import { CredentialService } from '../services/credential.service';


describe('ConnectComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        RouterTestingModule
      ],
      declarations: [
        ConnectComponent
      ],
      providers: [
        CredentialService
      ]
    }).compileComponents();
  }));

  it('should create the connect component', () => {
    const fixture = TestBed.createComponent(ConnectComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should render card header title as 'Connexion'`, () => {
    const fixture = TestBed.createComponent(ConnectComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.card-header').textContent).toContain('Connexion');
  });
});
