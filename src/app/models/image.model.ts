import * as uuid from 'uuid';

/**
 * Image model
 * @author Fabrice VAILLANT
 */
export class ImageModel {
    /**
     * Id of the image
     */
    id: string;

    /**
     * Image data url of the image
     */
    imageAsDataUrl: string;

    /**
     * Creates an instance of image model.
     * @author Fabrice VAILLANT
     * @param imageAsDataUrl : Image data url
     */
    constructor(imageAsDataUrl: string) {
        this.id = uuid.v4();
        this.imageAsDataUrl = imageAsDataUrl;
    }
}
