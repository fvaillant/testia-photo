/**
 * Angular
 */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

/**
 * Ng Bootstrap
 */
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

/**
 * Components
 */
import { ModalPhotoComponent } from './modal-photo/modal-photo.component';
import { AppComponent } from './app.component';
import { ConnectComponent } from './connect/connect.component';
import { GaleryComponent } from './galery/galery.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

/**
 * webcam module
 */
import { WebcamModule } from 'ngx-webcam';

/**
 * Ngx Webstorage
 */
import { NgxWebstorageModule } from 'ngx-webstorage';

/**
 * Routing
 */
import { AppRoutingModule } from './app-routing.module';

/**
 * Services
 */
import { CredentialService } from './services/credential.service';

/**
 * App Module
 * @author Fabrice VAILLANT
 */
@NgModule({
  declarations: [
    AppComponent,
    ConnectComponent,
    ModalPhotoComponent,
    GaleryComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    NgxWebstorageModule.forRoot({ prefix: 'todoApp', separator: '-' }),
    NgbModalModule,
    WebcamModule,
    AppRoutingModule
  ],
  providers: [CredentialService],
  exports: [],
  bootstrap: [AppComponent],
  entryComponents: [
    ModalPhotoComponent
  ]
})
export class AppModule { }
