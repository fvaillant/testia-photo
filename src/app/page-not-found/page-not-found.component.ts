/**
 * Angular
 */
import { Component, OnInit } from '@angular/core';

/**
 * Component for page not found
 * @author Fabrice VAILLANT
 */
@Component({
    selector: 'app-page-not-found',
    templateUrl: './page-not-found.component.html',
    styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit {

    /**
     * Creates an instance of page not found component.
     * @author Fabrice VAILLANT
     */
    constructor() {
    }

    /**
     * Init component
     * @author Fabrice VAILLANT
     */
    ngOnInit() {
    }
}
