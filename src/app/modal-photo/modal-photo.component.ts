/**
 * Angular
 */
import { Component } from '@angular/core';

/**
 * Rxjs
 */
import { Subject, Observable } from 'rxjs';

/**
 * Ng Bootstrap
 */
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

/**
 * Ngx Webcam
 */
import { WebcamImage, WebcamInitError } from 'ngx-webcam';

/**
 * Modal for adding an new image
 *   - Open webcam
 * @author Fabrice VAILLANT
 * @export
 */
@Component({
    selector: 'app-photo-modal',
    templateUrl: './modal-photo.component.html'
})
export class ModalPhotoComponent {
    /**
     * webcam snapshot trigger
     */
    trigger: Subject<void> = new Subject<void>();

    /**
     * Error message
     */
    errorMessage: string;

    /**
     * Creates an instance of modal photo component.
     * @author Fabrice VAILLANT
     * @param activeModal : Service for ngb modal
     */
    constructor(public activeModal: NgbActiveModal) {
    }

    /*******************/
    /*     EVENTS      */
    /*******************/

    /**
     * Event called when user clicks on cross or button close
     * @author Fabrice VAILLANT
     */
    close() {
        this.activeModal.dismiss();
    }

    /**
     * Handles image
     * @author Fabrice VAILLANT
     * @param webcamImage : New Webcam Image
     */
    handleImage(webcamImage: WebcamImage): void {
        this.activeModal.close(webcamImage);
    }

    /**
     * Handles init error
     * @author Fabrice VAILLANT
     */
    handleInitError(error: WebcamInitError): void {
        if (error.mediaStreamError && error.mediaStreamError.name === 'NotAllowedError') {
            this.errorMessage = 'L\'utilisation de la caméra n\'est pas autorisée par l\'utilisateur';
        }
    }

    /*******************/
    /* TRIGGERS WEBCAM */
    /*******************/

    /**
     * Triggers snapshot
     * @author Fabrice VAILLANT
     */
    public triggerSnapshot(): void {
        this.trigger.next();
    }

    /**
     * Gets trigger observable
     */
    public get triggerObservable(): Observable<void> {
        return this.trigger.asObservable();
    }
}
