import { TestBed, async } from '@angular/core/testing';

/**
 * webcam module
 */
import { WebcamModule } from 'ngx-webcam';

/**
 * Ng Bootstrap
 */
import { NgbModalModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

/**
 * Components
 */
import { ModalPhotoComponent } from './modal-photo.component';

describe('ModalPhotoComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        WebcamModule,
        NgbModalModule
      ],
      declarations: [
        ModalPhotoComponent
      ],
      providers: [
        NgbActiveModal
      ]
    }).compileComponents();
  }));

  it('should create the modal photo component', () => {
    const fixture = TestBed.createComponent(ModalPhotoComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should render header title as 'Ajouter une nouvelle photo'`, () => {
    const fixture = TestBed.createComponent(ModalPhotoComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.modal-title').textContent).toContain('Ajouter une nouvelle photo');
  });
});
