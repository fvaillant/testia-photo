/**
 * Angular
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/**
 * Components
 */
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { GaleryComponent } from './galery/galery.component';
import { ConnectComponent } from './connect/connect.component';

/**
 * Routes for app module
 */
const appRoutes: Routes = [
    { path: 'connect', component: ConnectComponent },
    { path: 'galery', component: GaleryComponent },
    { path: '', redirectTo: '/connect', pathMatch: 'full' },
    { path: '**', component: PageNotFoundComponent }
];

/**
 * App routing module
 * @author Fabrice VAILLANT
 */
@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes,
            { enableTracing: true } // <-- debugging purposes only
        )
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
