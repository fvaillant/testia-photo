# TestiaPhoto

Le projet a été généré avec [Angular CLI](https://github.com/angular/angular-cli) version 8.3.16.

## Design
Pour le style de l'application j'utilise scss, [Bootstrap](https://getbootstrap.com) et [Angular Bootstrap](https://ng-bootstrap.github.io/#/home).

## Webcam Library
Pour la gestion de la webcam ainsi que pour la prise de photo, j'utilise la librairie [Ngx Webcam](https://github.com/basst314/ngx-webcam).

## Electron
L'exécutable généré se trouve dans l'archive testia-photo-win32-x64.zip

## Autres informations
Identifiants de connexion pour tester l'application : 
admin / test
