/**
 * Angular
 */
import { Component } from '@angular/core';

/**
 * Ng Bootstrap
 */
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

/**
 * Components
 */
import { ModalPhotoComponent } from '../modal-photo/modal-photo.component';

/**
 * Ngx Webcam
 */
import { WebcamImage } from 'ngx-webcam';

/**
 * Models
 */
import { ImageModel } from '../models/image.model';

/**
 * Galery Component
 *  - Shows list of existing images
 *  - Events :
 *    - Add a new image
 *    - Remove an existing image
 * @author Fabrice VAILLANT
 */
@Component({
  selector: 'app-galery',
  templateUrl: './galery.component.html',
  styleUrls: ['./galery.component.scss']
})
export class GaleryComponent {
  /**
   * Contains all images
   */
  listImage: ImageModel[];

  /**
   * Creates an instance of galery component.
   * @author Fabrice VAILLANT
   * @param modalService : Modal Service for ngb-modal
   */
  constructor(protected modalService: NgbModal) {
    this.listImage = [];
  }

  /*******************/
  /*     EVENTS      */
  /*******************/

  /**
   * Adds new image
   * @author Fabrice VAILLANT
   */
  addNew() {
    const modalRef = this.modalService.open(ModalPhotoComponent, { backdrop: 'static', size: 'xl' });
    modalRef.result.then((webcamImage: WebcamImage) => {
      const image = new ImageModel(webcamImage.imageAsDataUrl);
      this.listImage.push(image);
    }, (reason) => {
      // Modal Dismissed --> Nothing to do
    });
  }

  /**
   * Removes existing image
   * @author Fabrice VAILLANT
   */
  removeImage(image: ImageModel) {
    const indexToRemove = this.listImage.findIndex((item) => item.id === image.id);
    if (indexToRemove > -1) {
      this.listImage.splice(indexToRemove, 1);
    }
  }
}
