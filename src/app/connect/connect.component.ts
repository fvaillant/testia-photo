/**
 * Angular
 */
import { Component } from '@angular/core';
import { Router } from '@angular/router';

/**
 * Servcices
 */
import { CredentialService } from '../services/credential.service';

/**
 * Component for connect a new user
 *   - Validate credentials
 * @author Fabrice VAILLANT
 */
@Component({
    selector: 'app-connect',
    templateUrl: './connect.component.html',
    styleUrls: ['./connect.component.scss']
})
export class ConnectComponent {
    /**
     * Login of the user
     */
    login: string;

    /**
     * Password of the user
     */
    password: string;

    /**
     * Error message
     */
    errorMessage: string;

    /**
     * Creates an instance of connect component.
     * @author Fabrice VAILLANT
     * @param router : Router Service
     * @param credentialService : Credentials service
     */
    constructor(private router: Router, private credentialService: CredentialService) {
        this.errorMessage = '';
    }

    /*******************/
    /*     EVENTS      */
    /*******************/

    /**
     * Connects a user
     * @author Fabrice VAILLANT
     */
    connect() {
        if (this.credentialService.validCrentials(this.login, this.password)) {
            // Redirect to galery component
            this.router.navigate(['/galery']);
        } else {
            // Show message error
            this.errorMessage = 'Utilisateur non reconnu!';
        }
    }
}
